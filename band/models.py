# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Band(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	image = models.ImageField(upload_to='products/images',null=True)
	bio = models.TextField(max_length=5000, null=True, blank=True)

	def __unicode__(self):
		return str(self.name)

	def __str__(self):
		return str(self.name)