# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView,ListView,DetailView
from django.shortcuts import render

from .models import Band

# Create your views here.
class BandList(ListView):
	template_name = 'band/index.html'
	queryset = Band.objects.all()
	model = Band

	def get_context_data(self, **kwargs):
	    context = super(BandList, self).get_context_data(**kwargs)
	    context['products'] = self.queryset
	    return context


class BandDetailView(DetailView):
	template_name = 'band/detail.html'
	model = Band

	def get_context_data(self, *args, **kwargs):
	    context = super(BandDetailView, self).get_context_data(*args, **kwargs)
	    context['now'] = timezone.now()
	    return context