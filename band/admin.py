# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Band
# Register your models here.

class BandAdmin(admin.ModelAdmin):
	#sets up values for how admin site lists categories
	list_display = ['name', 'timestamp', 'bio']
	list_display_links = ['name']
	ordering = ['name']
	search_fields = ['name']

	class Meta:
		model = Band

admin.site.register(Band,BandAdmin)