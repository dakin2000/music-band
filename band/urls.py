from django.conf.urls import url

from .views import BandList,BandDetailView

urlpatterns = [
    url(r'^$', BandList.as_view(), name='all_bands'),
    url(r'^detail/$', BandDetailView.as_view(), name='detail'),

]